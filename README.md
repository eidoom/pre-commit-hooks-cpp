# [pre-commit-hooks-cpp](https://gitlab.com/eidoom/pre-commit-hooks-cpp)

Usage is exemplified at [eidoom/cpp-ci](https://gitlab.com/eidoom/cpp-ci).

## Development

### TODO

* `clang-check` and `clang-tidy` fail because they require the compilation database file and this is not (currently) accessible from within the OCI image.

### Container

The image is stored in the [GitLab Container Registry](https://gitlab.com/eidoom/pre-commit-hooks-cpp/container_registry).

The image is automatically rebuilt by [GitLab CI](https://gitlab.com/eidoom/pre-commit-hooks-cpp/-/blob/main/.gitlab-ci.yml) when [`image/Containerfile`](https://gitlab.com/eidoom/pre-commit-hooks-cpp/-/blob/main/image/Containerfile) is edited.

Manual usage:

* Pull
	```shell
	podman pull registry.gitlab.com/eidoom/pre-commit-hooks-cpp
	```
* Run
	```shell
	podman run --rm registry.gitlab.com/eidoom/pre-commit-hooks-cpp <command>
	```
* Build
	```shell
	cd image
	podman build -t registry.gitlab.com/eidoom/pre-commit-hooks-cpp .
	```
* Push
	```shell
	podman push registry.gitlab.com/eidoom/pre-commit-hooks-cpp
	```
